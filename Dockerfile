FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > harfbuzz.log'

COPY harfbuzz .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' harfbuzz
RUN bash ./docker.sh

RUN rm --force --recursive harfbuzz
RUN rm --force --recursive docker.sh
RUN rm --force --recursive gcc

CMD harfbuzz
